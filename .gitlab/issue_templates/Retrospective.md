<!--

Before submitting the issue, replace:

- $RELEASE with the release number (e.g. 12.0).

Title: $RELEASE Retrospective

After submitting, start 3 discussions for with the following messages:

- `## What went well this release? :thumbsup:`
- `## What didn't go well this release? :thumbsdown:`
- `## What can we improve going forward? :chart_with_upwards_trend:`

-->

This is an asynchronous retrospective for $RELEASE. It's private to the Monitor group, plus anyone else that worked with the group during $RELEASE. This retrospective will be following the process described at
https://about.gitlab.com/handbook/engineering/management/team-retrospectives/

Please feel free to honestly describe your thoughts and feelings about working on that release below. I will add some starter discussions below, but you are
welcome to add further points. I will also be participating only as a moderator.

(If there is something you are not comfortable sharing, message me
directly. But note that 'Emotions are not only allowed in retrospectives, they
should be encouraged', so we'd love to hear from you here if possible.)

For reference:

* [Issues we shipped](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=closed&label_name[]=devops%3A%3Amonitor&milestone_title=$RELEASE)
* [Issues that slipped](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=devops%3A%3Amonitor&label_name%5B%5D=missed%3A$RELEASE)

/label ~retrospective
/confidential
/assign @akohlbecker @allison.browne @ameliabauerly @astoicescu @ck3g @ClemMakesApps @dbodicherla @dwilkins @jivanvl @k_cook @kencjohnston @lauraMon @mikolaj_wawrzyniak @mnohr @mrincon @nadia_sotnikova @ohoral @psimyn @rcobb @rpereira2 @sarahwaldner @seanarnold @splattael @svoegeli @syasonik @tristan.read
